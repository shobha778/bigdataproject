package com.bigdata.f15.modules.interfaces;

public interface Module {
	
	public String init(String[] parameters);
	
	public String start();
	
	public String getHelp();
	
}