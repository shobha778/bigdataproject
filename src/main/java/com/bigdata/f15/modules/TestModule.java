package com.bigdata.f15.modules;

import com.bigdata.f15.modules.interfaces.Module;

public class TestModule implements Module {

	
	
	@Override
	public String init(String[] parameters) {
		if(parameters.length < 1){
			return "Not enough parameters, specify number";
		}
		return null;
	}
	
	public String init(){
		
		return null;
	}

	@Override
	public String start() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHelp() {
		
		String helpString = "-n (Number of nodes)";
		
		return helpString;
	}
}